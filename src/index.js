import React from 'react';
import { render } from 'react-dom';


import App from './components/App';
// import './static/fonts/fonts.css';
import './static/style.css';

render(
    <App />,
    document.getElementById('root')
);