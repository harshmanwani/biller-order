import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


// const required = (value) => {
//     if (!value.toString().trim().length) {
//         // We can return string or jsx as the 'error' prop for the validated Component
//         return 'require';
//     }
// };

// const emailcheck = (value) => {
//     if (!validator.isEmail(value)) {
//         return `${value} is not a valid email.`
//     }
// };

class Info extends React.Component {
    constructor(props){
        super(props);
        
        this.isValidated = this.isValidated.bind(this);
        
    }

    // createOrder(event){
    //     event.preventDefault();

    // }

    isValidated() {
        // Conditions
        return true;
    }

    render(){
        return (
            <div className="steps">
                <div className="step-title">
                    <span className="hashtag">#</span>Personal Information
                </div>
                <form ref={c => { this.form = c }} className="info-box" autoComplete="off">
                    <div className="info-box-wrapper">
                        <FontAwesomeIcon
                            icon="phone"
                            color="#aaa"
                            className="info-box-icon"
                        />
                        <input ref={(input) => this.phone = input} type="number" placeholder="Mobile Number" name="phone" className="info-input"/>
                    </div>
                    <div  className="info-box-wrapper">
                        <FontAwesomeIcon
                            icon="user"
                            color="#aaa"
                            className="info-box-icon"
                        />
                        <input ref={(input) => this.name = input} type="text" placeholder="Name" name="name" className="info-input"/>
                    </div>
                    <div className="info-box-wrapper">
                        <FontAwesomeIcon
                            icon="envelope"
                            color="#aaa"
                            className="info-box-icon"
                        />
                        <input ref={(input) => this.email = input} type="email" placeholder="E-Mail" name="email" className="info-input"/>                        
                    </div>
                    <div className="info-box-wrapper">
                        <FontAwesomeIcon
                            icon="users"
                            color="#aaa"
                            className="info-box-icon"
                        />
                        <input ref={(input) => this.people = input} type="number" placeholder="No. of People" name="people" className="info-input"/>
                        <br/>
                        <div>
                            <label><input type="radio" name="tag" /><span className="tags">Family</span></label>
                            <label><input type="radio" name="tag" /><span className="tags">Couple</span></label>
                            <label><input type="radio" name="tag" /><span className="tags">Friends</span></label>
                            <br />
                            <label><input type="radio" name="tag" /><span className="tags">Work</span></label>
                            <label><input type="radio" name="tag" /><span className="tags">Individual</span></label>
                        </div>
                    </div>
                </form>

            </div>
        )
    }
}

// Custom form validate
// onBlur = { this.validationCheck }
// onBlur = { this.validationCheck }
// onBlur = { this.validationCheck }
// onBlur = { this.validationCheck }

export default Info