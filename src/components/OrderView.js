import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import '../static/orderView.css';

const OrderView = ({ toggleMenu }) => (
    <div className="orderView">
        <h2><span className="hashtag">#</span>Order</h2>
        
        {/* Order Cards */}

        <a className="addButton" onClick={toggleMenu}>
            <FontAwesomeIcon
                icon="plus"
                color="#fff"
                size="lg"
                className="plus"
            />
        </a>
    </div>
)

export default OrderView;