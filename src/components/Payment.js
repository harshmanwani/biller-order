import React from 'react';

import '../static/payments.css'

const Payment = () => (
    <div className="steps" >
        <div className="step-title">
            <span className="hashtag">#</span>Payment
        </div>
        <div className="promo-box">
            <label>
                <span className="promo-text">
                    Promo Code
                </span>
                <br/><input type="text" placeholder="Enter Promo code" className="enter-promo"/>
                <button className="apply-promo">Apply</button>
            </label>
        </div>
        <div className="container">
            <fieldset className="radiogroup">
                <legend>Select a Payment Method</legend>
                <ul className="radio">
                    <li>
                        <input type="radio" name="payment" id="cash" value="cash" />
                        <label htmlFor="cash" className="payment-method-label">Cash</label>
                    </li>
                    <li>
                        <input type="radio" name="payment" id="card" value="card" />
                        <label htmlFor="card" className="payment-method-label">Credit/Debit Card</label>
                        </li>
                    <li>
                        <input type="radio" name="payment" id="wallet" value="wallet" />
                        <label htmlFor="wallet" className="payment-method-label">Wallets</label>
                    </li>
                </ul>
            </fieldset>
        </div>    
    </div>
)

export default Payment