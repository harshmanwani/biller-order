import React from 'react';

const Logo = () => (
    <div className="logo">
        <img src="https://mybiller.io/img/logo-dark.png" alt=""/>
    </div>
);

export default Logo;