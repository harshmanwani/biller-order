
/* eslint-disable */

import React, {Component} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlus, faPhone, faUser, faUsers,faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { stack as Menu } from 'react-burger-menu';
import StepZilla from 'react-stepzilla';
import '../static/orderView.css';
import '../static/progTracker.css';

import Logo from './Logo';
import OrderView from './OrderView';
import Info from './Info';
import Order from './Order';
import Payment from './Payment';
import Confirm from './Confirm';

library.add(faPlus, faPhone, faUser, faUsers, faEnvelope);

const steps = [
    { name: 'DETAILS', component: <Info /> },
    { name: 'ORDER', component: <Order /> },
    { name: 'PAYMENT', component: <Payment /> },
    { name: 'CONFIRM', component: <Confirm /> }
];

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            menuOpen: false,
            order : {
                [`order-${Date.now()}`] : {
                    details : {},
                    items : [],
                }
            }
        }
    }

    showAddMenu(state) {
        this.setState({ menuOpen: state.isOpen })
    }

    toggleMenu() {
        this.setState({ menuOpen: true })
    }
    
    addOrder(){

    }

    render(){
        return (
            <div>
                <Logo/>
                <Menu
                    isOpen={this.state.menuOpen}
                    onStateChange={(state) => this.showAddMenu(state)}
                    right
                    width={'45vw'}
                    customBurgerIcon={false}
                    customCrossIcon={false}
                >
                    <div className='step-progress'>
                        <StepZilla 
                            steps={steps} 
                            prevBtnOnLastStep = {false}
                            nextButtonCls = "nextButton"
                            backButtonCls = "backButton"
                        />
                    </div>
                </Menu>
                <OrderView toggleMenu={() => this.toggleMenu()}/>
            </div>
        )
    }
}

export default App;